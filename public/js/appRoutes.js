angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {

    $routeProvider.

      when('/fat', {
        templateUrl: 'views/fat.html',
        controller: 'FatChartCtrl'
      }).
        when('/annotation', {
          templateUrl: 'views/annotation.html',
          controller: 'AnnotationChartCtrl'
        }).
        when('/generic/:chartType', {
          templateUrl: 'views/generic.html',
          controller: 'GenericChartCtrl'
        }).
        otherwise({
          redirectTo: '/fat'
        });

    /**

		// home page
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/nerds', {
			templateUrl: 'views/nerd.html',
			controller: 'NerdController'
		})

		.when('/geeks', {
			templateUrl: 'views/geek.html',
			controller: 'GeekController'	
		});
     */

	$locationProvider.html5Mode(true);

}]);